# Whats For Dinner

Whats For Dinner is a app for producing meal ideas, final submission data(title, desc, imgs.. etc) is under **Final Submission Data**. The code for the actual project is under **Project_files**

## Installation

1. Download the repository
2. Open **Project_files** with android studio
3. Click run and use an emulator or connected device to use the app

## Usage
If you are a new user click **Need to registter**, fill out all field(user img, username, email, password). On register it will send you to the homescreen. 
You will stay logged in till you sign out. 
On the home page you can swipe through the recipes(randomizes every time you change activvities, and will show the user that added them). When You see one you like click anywhere on it and it will show the full recipe.
You can then click **Cooked** and it will return you to the home screen and add the last recipe cooked to the end.
On the insert page you can fill out all the fiels and on save it will send it to the database. 
The settings page shows you a list of your own recipes that you can edit or delete from there. There is also a sign out.


## Contributing
Emmanuel Ogbonna (WIT)
Ryan Henige (cmich)

## Interaction Report
We used slack for communiation, bitbucket to share our code, and google drive to store our assets and project planning. Overall cross contetnental cooperation has been a success