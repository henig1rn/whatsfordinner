Title: 
	Whats For Dinner

Short Description: 
	Some times deciding what to eat is the hardest part

Long Description:
	Dinner can be one of the most important meals of the day. 
	It is a time for families to release the days issues and 
	come together and relax. You dont want to add any stress 
	to it, and from my own experience coming up with what to
	eat is the hardest part. With this app that became easier.
	Look through other people recipes, add your own, and sort
	out the stuff you don't like. Helps you include and remove 
	ingredient contents that you like or dont like or might 
	have an allegric reaction to.

Tags:
	recipes, recipe book, food, planner, dinner, ideas