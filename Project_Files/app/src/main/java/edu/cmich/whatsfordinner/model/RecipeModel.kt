package edu.cmich.whatsfordinner.model


data class RecipeModel(
                           //basic info
    var uuid: String = "",
    var uId: String = "",
    var img: String = "",
    var title: String = "",
    var cookTime: String = "",
    var dishType: String = "",
    //just making them strings for now
    var ingredients: String = "",
    var steps: String = "",
//    var ingredients: List<String> = emptyList(),
//    var steps: List<String> = emptyList(),
    //info for algorithm that will be boolean
    var nuts: Boolean = false,
    var vegetarian: Boolean = false,
    var gluten: Boolean = false,
    var shellFish: Boolean = false
){
    constructor() : this("","","","","","","","",false,false, false,false)
}


