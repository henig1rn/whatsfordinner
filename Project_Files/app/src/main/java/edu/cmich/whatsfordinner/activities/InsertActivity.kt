package edu.cmich.whatsfordinner.activities

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import edu.cmich.whatsfordinner.R
import edu.cmich.whatsfordinner.model.RecipeModel
import kotlinx.android.synthetic.main.activity_insert.*
import org.jetbrains.anko.toast
import java.util.*

@Suppress("DEPRECATION")
class InsertActivity : AppCompatActivity() {
    private var ingredientsList: String = ""
    private var stepsList: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_insert)

        btn_save.setOnClickListener() {
            // get data and send to firebase
            uploadToFirebaseStorage()
        }

        //spinner
        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.type_array, android.R.layout.simple_spinner_item
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        insert_spinner.adapter = adapter

         //img on click
        constraintLayoutimg.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 0 )
        }

        ingredientAdd.setOnClickListener{
            addIngredient()
        }
        stepAdd.setOnClickListener{
            addStep()
        }

        var deleteStep = deleteSteps
        var insertSt = insert_steps
        deleteStep.setOnClickListener {
            stepsList = ""
            insertSt.text = stepsList
            insertSt.hint = "Steps..."
            i = 0
        }

        var insertIng = insert_ingredients
        var delete = deleteIng
        delete.setOnClickListener {
            ingredientsList = ""
            insertIng.text = ingredientsList
            insertIng.hint = "Ingredients.."
        }

        back_arrow.setOnClickListener() {
            //takes back to home screen
            goHome()
        }

    }
    var i = 0
    private fun addStep() {
        val stepBox = step.text
        var insert = insert_steps

        if(stepBox.isNotEmpty()){
            i++
                stepsList +=  "Step ${i}:  $stepBox \n"
        }else{
            toast("There was nothing to add")
        }
        stepBox.clear()
        insert.text = stepsList
    }

    private fun addIngredient() {
        val ingredientBox = ingredient.text
        var insert = insert_ingredients
        if(ingredientBox.isNotEmpty()){
            if(ingredientsList.length > 1){
                ingredientsList +=  ", $ingredientBox"
            }else{
                ingredientsList +=  " $ingredientBox"
            }
        }else{
            toast("There was nothing to add")
        }

        ingredientBox.clear()
        insert.text = ingredientsList
    }

    fun goHome() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
    }

    var selectedPhoto: Uri? = null
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 0 && resultCode == Activity.RESULT_OK && data != null){
            selectedPhoto = data.data
            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, selectedPhoto)
            insert_img.setImageBitmap(bitmap)
            constraintLayoutimg.alpha = 0f
        }
    }

    private fun uploadToFirebaseStorage(){

        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/images/$filename")
        if(selectedPhoto == null){
            toast("You need to add an image")
        }else{
            ref.putFile(selectedPhoto!!).addOnSuccessListener {
                ref.downloadUrl.addOnSuccessListener {
                    val link = it.toString()
                    setData(link)
                }.addOnFailureListener{
                    Log.d("download url", "failed to get download")
                }
            }
        }
    }

    private fun setData(profileImageUrl: String) {

        //get data and add to model
        val id = UUID.randomUUID().toString()
        val uid = FirebaseAuth.getInstance().uid ?: ""
        val title = insert_title.text.toString()
        val cookTime = insert_time.text.toString()
        val dishType = insert_spinner.selectedItem.toString()
        val ingredients = ingredientsList
        val steps = stepsList
        //checkbox values
        val nuts = nuts_checkBox.isChecked
        val gluten = gluten_checkBox.isChecked
        val vegetarian = vegetarian_checkBox.isChecked
        val shellFish = shellFish_checkBox.isChecked

        if(title.isEmpty() || cookTime.isEmpty() || dishType.isEmpty() || ingredients.isEmpty() || steps.isEmpty() || ingredients.isEmpty()
        ){
            toast("You are missing something")
        }else{
            val ref = FirebaseDatabase.getInstance().getReference("/recipes/$id")
            val recipe = RecipeModel(id, uid, profileImageUrl, title, cookTime, dishType, ingredients, steps, nuts, vegetarian, gluten, shellFish )
            ref.setValue(recipe)
            //takes back to home screen
            goHome()
        }
    }
}

