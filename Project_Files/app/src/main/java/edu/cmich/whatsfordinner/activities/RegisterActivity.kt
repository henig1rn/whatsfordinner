package edu.cmich.whatsfordinner.activities

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage

import edu.cmich.whatsfordinner.R
import kotlinx.android.synthetic.main.activity_insert.*
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_register.constraintLayoutimg
import kotlinx.android.synthetic.main.activity_register.email_edittext_register
import kotlinx.android.synthetic.main.activity_register.insert_img
import org.jetbrains.anko.toast
import java.util.*


class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        register_button_register.setOnClickListener {
            uploadToFirebaseStorage()
        }

        already_have_account_textview.setOnClickListener {
            // launch the login activity somehow
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
        constraintLayoutimg.setOnClickListener{
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 0 )
        }
    }
    var selectedPhoto: Uri? = null
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 0 && resultCode == Activity.RESULT_OK && data != null){
            selectedPhoto = data.data
            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, selectedPhoto)
            insert_img.setImageBitmap(bitmap)
            constraintLayoutimg.alpha = 0f
        }
    }

    private fun uploadToFirebaseStorage(){

        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/images/$filename")
        if(selectedPhoto == null){
            toast("You need to add a image")
        }else{
            ref.putFile(selectedPhoto!!).addOnSuccessListener {
                ref.downloadUrl.addOnSuccessListener {
                    val link = it.toString()
                    register(link)
                }.addOnFailureListener{
                    Log.d("download url", "failed to get download")
                }
            }
        }
    }

    private fun register(profileImageUrl: String) {
        val email = email_edittext_register.text.toString()
        val password = password_edittext_register.text.toString()
        val username = username_register.text.toString()
        val emailValidate = "^[A-Za-z](.*)([@]{1})(.{1,})(\\.)(.{1,})";
        if(!emailValidate.toRegex().matches(email)){
            toast("Email is not recognized")
            return
        }
        if(email.isEmpty() || password.isEmpty() || username.isEmpty()){
            toast("Please enter all information")
            return
        }
        if(password.length < 6){
            toast("Password needs to be a minimum of 6 characters")
            return
        }
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener{
                if (!it.isComplete) return@addOnCompleteListener
                toast("Creating your account")
                saveUserToFirebaseDatabase(profileImageUrl)
            }.addOnFailureListener{
                toast("Failed to create User")
            }
    }

    private fun saveUserToFirebaseDatabase(profileImageUrl: String) {
        val uid = FirebaseAuth.getInstance().uid ?: ""
        val ref = FirebaseDatabase.getInstance().getReference("/users/$uid")

        val user = User(uid, profileImageUrl, username_register.text.toString(), email_edittext_register.text.toString(), "")
        ref.setValue(user).addOnSuccessListener {
            Log.d("Register", "Saved to database")
            val intent = Intent(this, HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }
}

data class User(
    val uid: String,
    val img: String,
    val username: String,
    val email: String,
    val lastCooked: String
){
    constructor() : this("","", "","","")
}