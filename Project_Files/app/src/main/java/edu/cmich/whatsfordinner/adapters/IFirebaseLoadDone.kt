package edu.cmich.whatsfordinner.adapters

import edu.cmich.whatsfordinner.model.RecipeModel

interface IFirebaseLoadDone {
    fun onFirebaseLoadSuccess(recipeList:List<RecipeModel>)
    fun onFirebaseLoadFail(message:String)
}