package edu.cmich.whatsfordinner.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth

import com.google.firebase.database.*
import edu.cmich.whatsfordinner.R
import edu.cmich.whatsfordinner.adapters.IFirebaseLoadDone
import edu.cmich.whatsfordinner.adapters.RecipeAdapter
import edu.cmich.whatsfordinner.model.RecipeModel
import kotlinx.android.synthetic.main.activity_home.*
import org.jetbrains.anko.toast

class HomeActivity : AppCompatActivity(), IFirebaseLoadDone {
    override fun onFirebaseLoadSuccess(recipeList: List<RecipeModel>) {
        adapter = RecipeAdapter(this, recipeList, lastCooked, Glide.with(this))
        view_pager.adapter = adapter
    }

    override fun onFirebaseLoadFail(message: String) {
        toast("Fail")
    }

    lateinit var adapter: RecipeAdapter
    lateinit var recipes: DatabaseReference
    lateinit var users: DatabaseReference
    lateinit var userAllergies: DatabaseReference
    lateinit var iFirebaseLoadDone: IFirebaseLoadDone
    lateinit var lastCooked: String
    lateinit var allergies: Allergies
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val uid = FirebaseAuth.getInstance().uid ?: ""
        if(!::allergies.isInitialized){
            allergies = Allergies(
                nuts = false,
                vegetarian = false,
                gluten = false,
                shellFish = false
            )
        }
        if(!::lastCooked.isInitialized){
            lastCooked = ""
        }
        // no recipes
        none.alpha = 0f

        // These are for the buttons to switch pages
        val btnSetting = findViewById<ImageView>(R.id.btn_setting)
        val btnInsert = findViewById<ImageView>(R.id.btn_insert)
        val btnRefresh = findViewById<ImageView>(R.id.btn_refresh)
        // go to settings
        btnSetting.setOnClickListener(){
            val intent = Intent(this, SettingActivity::class.java)
            this.startActivity(intent)
        }
        //got to insert page
        btnInsert.setOnClickListener(){
            val intent = Intent(this, InsertActivity::class.java)
            this.startActivity(intent)
        }
        btnRefresh.setOnClickListener {
            finish()
            startActivity(getIntent())
        }
        iFirebaseLoadDone = this

        recipes = FirebaseDatabase.getInstance().getReference("recipes")
        users = FirebaseDatabase.getInstance().getReference("users/$uid")
        getUser()
        getUserAllergies(uid)
        getData()

    }

    private fun getUser() {
        users.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
            }
            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()) {
                    var user = p0.getValue(User::class.java)
                    lastCooked = user?.lastCooked.toString()
                }
            }
        })
    }

    private fun getUserAllergies(uid: String) {
        userAllergies = FirebaseDatabase.getInstance().getReference("users/$uid/allergies")

        userAllergies.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
            }
            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()) {
                    allergies = p0.getValue(Allergies::class.java)!!
                }
            }
        })
    }

    private fun getData() {
        recipes.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onDataChange(p0: DataSnapshot) {
                if(p0.exists()){
                    val recipeList = ArrayList<RecipeModel>()
                    val sortedList = ArrayList<RecipeModel>()

                    recipeList.clear()
                    for(h in p0.children){
                        val recipe = h.getValue(RecipeModel::class.java)
                        recipeList.add(recipe!!)
                    }
                    recipeList.shuffle()
                    // Moved the users last cooked item to end of list
                    for (i in recipeList.indices){
                        if(recipeList[i].uuid == lastCooked){
                            var recipe = recipeList[i]
                            recipeList.removeAt(i)
                            recipeList.add(recipe)
                        }

                        if((!recipeList[i].nuts || !allergies.nuts) && (!recipeList[i].gluten || !allergies?.gluten)
                            && (!recipeList[i].shellFish || !allergies?.shellFish) && (!recipeList[i].vegetarian || !allergies?.vegetarian)){
                            sortedList.add(recipeList[i])
                        }
                        if(sortedList.isEmpty()){
                            none.alpha = 1f
                        }
                    }
                    iFirebaseLoadDone.onFirebaseLoadSuccess(sortedList)
                }
            }
        })
    }
}

