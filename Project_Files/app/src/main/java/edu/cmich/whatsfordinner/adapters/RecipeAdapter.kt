package edu.cmich.whatsfordinner.adapters

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.Intent.getIntent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.GlideContext
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import edu.cmich.whatsfordinner.R
import edu.cmich.whatsfordinner.activities.HomeActivity
import edu.cmich.whatsfordinner.activities.User
import edu.cmich.whatsfordinner.model.RecipeModel
import kotlinx.android.synthetic.main.activity_setting.*


class RecipeAdapter(val mCtx: Context, val recipeList: List<RecipeModel>, val lastcooked: String, val glide: RequestManager)
    : PagerAdapter(){

    internal var inflater:LayoutInflater;
    lateinit var users: DatabaseReference
    init{
        inflater = LayoutInflater.from(mCtx)
    }

    override fun isViewFromObject(view: View, `o`: Any): Boolean {
        return view == o;
    }

    override fun getCount(): Int {
        return recipeList.size
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        (container as ViewPager).removeView(`object` as View)
    }


    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        val textViewName = view.findViewById<TextView>(R.id.recipe_title)
        var recipeImage = view.findViewById<ImageView>(R.id.recipe_image)
        var textTime = view.findViewById<TextView>(R.id.recipe_time)
        var textType = view.findViewById<TextView>(R.id.recipe_type)
        var lastCooked = view.findViewById<ImageView>(R.id.lastcooked)
        val userimg = view.findViewById<ImageView>(R.id.user_img)

        textViewName.text = recipeList[position].title
        Glide.with(recipeImage).load(recipeList[position].img).transform( RoundedCorners(20)).into(recipeImage)
        textTime.text = recipeList[position].cookTime + " Min"
        textType.text = recipeList[position].dishType
        lastCooked.visibility = View.INVISIBLE

        if(recipeList[position].uuid == lastcooked){
            lastCooked.visibility = View.VISIBLE
        }

        // get user
        val uid = recipeList[position].uId
        val cUid = FirebaseAuth.getInstance().uid ?: ""
        getUser(uid, userimg)

        // when an item is clicked it will display full recipe
        view.setOnClickListener(){
            val builder = AlertDialog.Builder(mCtx, android.R.style.Theme_DeviceDefault_NoActionBar_Fullscreen)
            val inflater = LayoutInflater.from(mCtx)
            val view = inflater.inflate(R.layout.show_recipe,null)

            //grab content views
            val title = view.findViewById<TextView>(R.id.show_title)
            val img = view.findViewById<ImageView>(R.id.show_img)
            val time = view.findViewById<TextView>(R.id.show_time)
            val type = view.findViewById<TextView>(R.id.show_spinner)
            val ingredients = view.findViewById<TextView>(R.id.show_ingredients)
            val steps = view.findViewById<TextView>(R.id.show_steps)
            val nuts = view.findViewById<CheckBox>(R.id.nuts_checkBox)
            val gluten = view.findViewById<CheckBox>(R.id.gluten_checkBox)
            val vegetarian = view.findViewById<CheckBox>(R.id.vegetarian_checkBox)
            val shellFish = view.findViewById<CheckBox>(R.id.shellFish_checkBox)
            val close = view.findViewById<ImageView>(R.id.back_arrow)
            val cooked = view.findViewById<ImageView>(R.id.btn_cooked)

            // grab from model and display
            Glide.with(img).load(recipeList[position].img).into(img)
            title.text = recipeList[position].title
            time.text = recipeList[position].cookTime
            type.text = recipeList[position].dishType
            ingredients.text = recipeList[position].ingredients
            steps.text = recipeList[position].steps
            nuts.isChecked = recipeList[position].nuts
            gluten.isChecked = recipeList[position].gluten
            vegetarian.isChecked = recipeList[position].vegetarian
            shellFish.isChecked = recipeList[position].shellFish
            builder.setView(view)

            // builds dialog and shows
            val alert = builder.create()
            alert.show()

            // close out without changes
            close.setOnClickListener{
                alert.dismiss()
            }
            //this will update list view
            cooked.setOnClickListener{
                val ref = FirebaseDatabase.getInstance().getReference("/users/${cUid}/lastCooked")

                val lastCooked = recipeList[position].uuid
                ref.setValue(lastCooked).addOnSuccessListener {
                    val intent = Intent(mCtx, HomeActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                    mCtx.startActivity(intent)
                    alert.dismiss()
                }
            }
        }
        container.addView(view)
        return view
    }

    private fun getUser(uid: String, userImg: ImageView) {
        users = FirebaseDatabase.getInstance().getReference("users/$uid")

        users.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
            }
            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()) {
                    var user = p0.getValue(User::class.java)
                    if(user?.img != null || user?.img != ""){
                        if (mCtx != null){
                            glide.load(user?.img).into(userImg)
                        }
                    }
                }
            }
        })
    }


}