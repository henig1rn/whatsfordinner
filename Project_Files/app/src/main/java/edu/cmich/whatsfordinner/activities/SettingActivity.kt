package edu.cmich.whatsfordinner.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import edu.cmich.whatsfordinner.R
import edu.cmich.whatsfordinner.adapters.myRecipeAdapter
import edu.cmich.whatsfordinner.model.RecipeModel
import kotlinx.android.synthetic.main.activity_setting.*
import kotlinx.android.synthetic.main.activity_setting.back_arrow
import kotlinx.android.synthetic.main.activity_setting.btn_save
import kotlinx.android.synthetic.main.show_recipe.*

import kotlin.collections.ArrayList

class SettingActivity : AppCompatActivity() {

    lateinit var ref : DatabaseReference
    lateinit var listView: ListView
    lateinit var users: DatabaseReference
    lateinit var userAllergies: DatabaseReference


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        listView = findViewById(R.id.listView)
        val uid = FirebaseAuth.getInstance().uid ?: ""
        var context = this
        //functions
        getUser(uid)
        getUserAllergies(uid)
        getList(uid, context)


        btn_save.setOnClickListener{
            saveAllergyInfo()
        }

        back_arrow.setOnClickListener{
            val intent = Intent(this, HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }

        signOutBtn.setOnClickListener{
            signOut()
        }
    }

    private fun getUser(uid: String) {
        users = FirebaseDatabase.getInstance().getReference("users/$uid")

        users.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
            }
            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()) {
                    var user = p0.getValue(User::class.java)
                    uidName.text = "Welcome ${user?.username}"
                }
            }
        })
    }

    private fun getUserAllergies(uid: String) {
        userAllergies = FirebaseDatabase.getInstance().getReference("users/$uid/allergies")

        userAllergies.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
            }
            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()) {
                    var allergies = p0.getValue(Allergies::class.java)
                    nuts_settings.isChecked = allergies?.nuts!!
                    gluten_settings.isChecked = allergies?.gluten!!
                    vegetarian_settings.isChecked = allergies?.vegetarian!!
                    shellFish_settings.isChecked = allergies?.shellFish!!
                }
            }
        })
    }

    private fun getList(uid: String, context: Context) {
        ref = FirebaseDatabase.getInstance().getReference("recipes")


        ref.addValueEventListener(object: ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
            }
            override fun onDataChange(p0: DataSnapshot) {
                if(p0.exists()){
                    val myRecipeList = ArrayList<RecipeModel>()
                    var myList = ArrayList<RecipeModel>()

                    myRecipeList.clear()
                    for (h in p0.children){
                        val recipe = h.getValue(RecipeModel::class.java)
                        myRecipeList.add(recipe!!)
                    }

                    // sorts the list alphabetically
                    var sortedList = myRecipeList.sortedWith( compareBy {
                        it.title
                    })
                    //creates a new list and adds only the users recipes
                    for (i in sortedList.indices){
                        if (uid == myRecipeList[i].uId){
                            myList.add(myRecipeList[i])
                        }
                    }
                    val adapter = myRecipeAdapter(context, R.layout.myrecipe_layout, myList)

                    listView.adapter = adapter
                }
            }
        })    }

    private fun saveAllergyInfo() {
        val nuts = nuts_settings.isChecked
        val gluten = gluten_settings.isChecked
        val vegetarian = vegetarian_settings.isChecked
        val shellFish = shellFish_settings.isChecked

        val uid = FirebaseAuth.getInstance().uid ?: ""
        val ref = FirebaseDatabase.getInstance().getReference("/users/${uid}/allergies")

        val allergies = Allergies(nuts, vegetarian, gluten, shellFish)
        ref.setValue(allergies).addOnSuccessListener {
            Log.d("Setttings", "Saved to database")
            val intent = Intent(this, HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }

    private fun signOut() {
        FirebaseAuth.getInstance().signOut()
        val intent = Intent(this, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }
}

data class Allergies(
    var nuts: Boolean = false,
    var vegetarian: Boolean = false,
    var gluten: Boolean = false,
    var shellFish: Boolean = false
)