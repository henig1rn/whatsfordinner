package edu.cmich.whatsfordinner.adapters


import android.app.AlertDialog
import android.content.Context
import android.text.Editable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import edu.cmich.whatsfordinner.R
import edu.cmich.whatsfordinner.model.RecipeModel


class myRecipeAdapter(val mCtx: Context, val layoutResId: Int, val recipeList: List<RecipeModel>)
    : ArrayAdapter<RecipeModel>(mCtx, layoutResId, recipeList){

    private var ingredientsList: String = ""
    private var stepsList: String = ""
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater : LayoutInflater = LayoutInflater.from(mCtx)
        val view: View = layoutInflater.inflate(layoutResId, null)
        val uid = FirebaseAuth.getInstance().uid ?: ""

        val updateBtn = view.findViewById<ImageView>(R.id.update_btn)
        val deleteBtn = view.findViewById<ImageView>(R.id.delete_btn)

        updateBtn.visibility = View.INVISIBLE
        deleteBtn.visibility = View.INVISIBLE

        val recipeName = view.findViewById<TextView>(R.id.recipe_name)
        val recipe = recipeList[position]

//        //update recipe time
//        val recipe :TextView! = view.findViewById<TextView><edu.cmich.whatsfordinner.R.id.recipetimeupdate)
//
//        recipe.setOnclicklistener {
//            showUpdateDialog()
//        }
//
//        fun showUpdateDialog()(
//
//        )

        recipeName.text = recipe.title
        if(uid == recipe.uId){
            updateBtn.visibility = View.VISIBLE
            deleteBtn.visibility = View.VISIBLE
        }

        updateBtn.setOnClickListener {
            updateIt(recipe)
        }
        deleteBtn.setOnClickListener {
            dialog(recipe)

        }
        return view
    }

    private fun dialog(recipe: RecipeModel) {
        val builder = AlertDialog.Builder(mCtx)
        builder.setTitle("Are you sure you want to delete?")
        builder.setMessage("This can not be undone")

        builder.setPositiveButton("YES"){dialog, which ->
            val myDatabase = FirebaseDatabase.getInstance().getReference("recipes")
            myDatabase.child(recipe.uuid).removeValue()
            dialog.dismiss()
        }
        builder.setNegativeButton("No"){dialog,which ->
            dialog.dismiss()
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun updateIt(recipe: RecipeModel) {
        val builder = AlertDialog.Builder(mCtx, android.R.style.Theme_DeviceDefault)
        val inflater = LayoutInflater.from(mCtx)
        val view = inflater.inflate(R.layout.activity_insert,null)

        // Spinner
        var insertSpinner = view.findViewById<Spinner>(R.id.insert_spinner)
        val spinnerAdapter = createFromResource(
            mCtx, R.array.type_array, android.R.layout.simple_spinner_item
        )
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        insertSpinner.adapter = spinnerAdapter

        //grab content views
        val title = view.findViewById<TextView>(R.id.insert_title)
        val img = view.findViewById<ImageView>(R.id.insert_img)
        val imgbtn = view.findViewById<Button>(R.id.constraintLayoutimg)
        val time = view.findViewById<TextView>(R.id.insert_time)
        val ingredients = view.findViewById<TextView>(R.id.insert_ingredients)
        val steps = view.findViewById<TextView>(R.id.insert_steps)
        val nuts = view.findViewById<CheckBox>(R.id.nuts_checkBox)
        val gluten = view.findViewById<CheckBox>(R.id.gluten_checkBox)
        val vegetarian = view.findViewById<CheckBox>(R.id.vegetarian_checkBox)
        val shellFish = view.findViewById<CheckBox>(R.id.shellFish_checkBox)
        val close = view.findViewById<ImageView>(R.id.back_arrow)
        val save = view.findViewById<ImageView>(R.id.btn_save)
        imgbtn.alpha = 0f
        imgbtn.setOnClickListener{
            Toast.makeText(mCtx,"The image can not be changed", Toast.LENGTH_LONG).show()
        }
        // grab from model and display
        Glide.with(img).load(recipe.img).into(img)
        title.text = recipe.title
        time.text = recipe.cookTime
        insertSpinner.setSelection(spinnerAdapter.getPosition(recipe.dishType))
        ingredients.text = recipe.ingredients
        steps.text = recipe.steps
        nuts.isChecked = recipe.nuts
        gluten.isChecked = recipe.gluten
        vegetarian.isChecked = recipe.vegetarian
        shellFish.isChecked = recipe.shellFish
        builder.setView(view)

        // builds dialog and shows
        val alert = builder.create()
        alert.show()

        // close out without changes
        close.setOnClickListener{
            alert.dismiss()
        }
        ingredientsList = recipe.ingredients
        stepsList = recipe.steps

        // steps & ingredients
        val stepBox = view.findViewById<EditText>(R.id.step).text
        var deleteStep = view.findViewById<ImageView>(R.id.deleteSteps)
        var insertSteps = view.findViewById<TextView>(R.id.insert_steps)
        var stepAdd = view.findViewById<Button>(R.id.stepAdd)


        val ingredientBox = view.findViewById<EditText>(R.id.ingredient).text
        var deleteIng = view.findViewById<ImageView>(R.id.deleteIng)
        var insertIngredients= view.findViewById<TextView>(R.id.insert_ingredients)
        var ingredientAdd = view.findViewById<Button>(R.id.ingredientAdd)

        ingredientAdd.setOnClickListener{
            addIngredient(ingredientBox, insertIngredients)

        }
        stepAdd.setOnClickListener{
            addStep(stepBox, insertSteps)
        }

        deleteStep.setOnClickListener {
            stepsList = ""
            insertSteps.text = stepsList
            i = 0
        }

        deleteIng.setOnClickListener {
            ingredientsList = ""
            insertIngredients.text = ingredientsList
        }

        //this will update list view
        save.setOnClickListener{
                //get data and add to model
                val id = recipe.uuid
                val profileImageUrl = recipe.img
                val uid = FirebaseAuth.getInstance().uid ?: ""
                val title = title.text.toString()
                val cookTime = time.text.toString()
                val dishType = insertSpinner.selectedItem.toString()
                val ingredients = ingredientsList
                val steps = stepsList
                //checkbox values
                val nuts = nuts.isChecked
                val gluten = gluten.isChecked
                val vegetarian = vegetarian.isChecked
                val shellFish = shellFish.isChecked

            if(title.isEmpty() || cookTime.isEmpty() || dishType.isEmpty() || ingredients.isEmpty() || steps.isEmpty() || ingredients.isEmpty()
            ){
                Toast.makeText(mCtx,"You are missing something", Toast.LENGTH_LONG).show()
            }else{
                val ref = FirebaseDatabase.getInstance().getReference("/recipes/$id")
                val recipe = RecipeModel(id, uid, profileImageUrl, title, cookTime, dishType, ingredients, steps, nuts, vegetarian, gluten, shellFish )
                ref.setValue(recipe)
                alert.dismiss()
            }
        }
    }

    var i = 0
    private fun addStep(stepBox: Editable, insert: TextView) {

        if(stepBox.isNotEmpty()){
            i++
            stepsList +=  "Step ${i}:  $stepBox \n"
        }
        stepBox.clear()
        insert.text = stepsList
    }

    private fun addIngredient(ingredientBox: Editable, insert: TextView) {
        if(ingredientBox.isNotEmpty()){
            if(ingredientsList.length > 1){
                ingredientsList +=  ", $ingredientBox"
            }else{
                ingredientsList +=  " $ingredientBox"
            }
        }
        ingredientBox.clear()
        insert.text = ingredientsList
    }
}



